// player
var elementplayerrock = document.getElementById('player1_rock');
var elementplayerpaper = document.getElementById('player1_paper');
var elementplayerscissors = document.getElementById('player1_scissors');
// computer
var elementcomrock = document.getElementById('com_rock');
var elementcompaper = document.getElementById('com_paper');
var elementcomscissors = document.getElementById('com_scissors');
var com = ['rock', 'paper', 'scissors'];
//  function param player
function playgame(param){
   console.log(param)
   elementplayerrock.classList.remove("active");
   elementplayerpaper.classList.remove("active");
   elementplayerscissors.classList.remove("active");
   // logic
   if(param =="rock"){
      elementplayerrock.classList.add("active");
   }else
   if(param =="paper"){
      elementplayerpaper.classList.add("active");
   }else
   if(param =="scissors"){
      elementplayerscissors.classList.add("active");
   }

   var pilihan_com = random_item(com);
   console.log(pilihan_com, 'PILIHAN COM')

   elementcomrock.classList.remove("active");
   elementcompaper.classList.remove("active");
   elementcomscissors.classList.remove("active");

   // logic
   if(pilihan_com =="rock"){
      elementcomrock.classList.add("active");
   }else
   if(pilihan_com =="paper"){
      elementcompaper.classList.add("active");
   }else
   if(pilihan_com =="scissors"){
      elementcomscissors.classList.add("active");
   }
   if(param == pilihan_com){
      console.log('draw')
      document.getElementById('content_result').innerHTML = '<div style="width:100%;text-align:center">DRAW</div>';
      document.getElementById('content_result').classList.remove('result_game')
      document.getElementById('content_result').classList.add('result_game')
   }else
   if(param == 'scissors' && pilihan_com == 'paper'){
      console.log('PLAYER 1 WIN')
      document.getElementById('content_result').innerHTML = '<div style="width:100%;text-align:center">PLAYER 1 WIN</div>';
      document.getElementById('content_result').classList.remove('result_game')
      document.getElementById('content_result').classList.add('result_game')
   }else
   if(param == 'scissors' && pilihan_com == 'rock'){
      console.log('COM WIN')
      document.getElementById('content_result').innerHTML = '<div style="width:100%;text-align:center">COM WIN</div>';
      document.getElementById('content_result').classList.remove('result_game')
      document.getElementById('content_result').classList.add('result_game')
   }
   else
   if(param == 'paper' && pilihan_com == 'rock'){
      console.log('PLAYER 1 WIN')
      document.getElementById('content_result').innerHTML = '<div style="width:100%;text-align:center">PLAYER 1 WIN</div>';
      document.getElementById('content_result').classList.remove('result_game')
      document.getElementById('content_result').classList.add('result_game')
   }else
   if(param == 'paper' && pilihan_com == 'scissors'){
      console.log('COM WIN')
      document.getElementById('content_result').innerHTML = '<div style="width:100%;text-align:center">COM WIN</div>';
      document.getElementById('content_result').classList.remove('result_game')
      document.getElementById('content_result').classList.add('result_game')
   }
   else
   if(param == 'rock' && pilihan_com == 'scissors'){
      console.log('COM WIN')    
      document.getElementById('content_result').innerHTML = '<div style="width:100%;text-align:center">COM WIN</div>';
      document.getElementById('content_result').classList.remove('result_game')
      document.getElementById('content_result').classList.add('result_game')
   } 
   else
   if(param == 'rock' && pilihan_com == 'paper'){
      console.log('PLAYER 1 WIN')
      document.getElementById('content_result').innerHTML = '<div style="width:100%;text-align:center">PLAYER 1 WIN</div>';
      document.getElementById('content_result').classList.remove('result_game')
      document.getElementById('content_result').classList.add('result_game')
   }           
}

// function random computer
function random_item(items)
{       
    var index = Math.floor(Math.random()*items.length);
    return items[index];
}
// refresh versus
function refresh(){
   document.getElementById('content_result').innerHTML = 'vs';
   document.getElementById('content_result').classList.remove('result_game')
   
   // com 
   elementcomrock.classList.remove("active");
   elementcompaper.classList.remove("active");
   elementcomscissors.classList.remove("active");
   // player
   elementplayerrock.classList.remove("active");
   elementplayerpaper.classList.remove("active");
   elementplayerscissors.classList.remove("active");
}
